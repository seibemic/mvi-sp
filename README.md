# mvi-sp
**Rozložení klávesnice pro český jazyk vytvořené
genetickým algoritmem**


V této práci je úkolem pomocí genetického algoritmu vytvořit optimální rozložení klávesnice pro český jazyk. K tomu jsou použita data dvojího typu - pohádky od K. J. Erbena a odborného textu, který je získán z abstraktů z bakalářských a diplomových prací na FIT ČVUT.

Genetický algoritmus je navržen tak, aby minimalizoval pohyb prstů a zajistil co možná největší střídání prstů.

Veškerá data jsou obsažena ve složce /src/Data.

Genetický algoritmus je implementován v Jupyter notebooku pod názvem geneticAlgorithm.ipynb ve složce src. V notebooku jsou obsaženy všechny výstupy, jeho spuštění není doporučené - trvá několik hodin pro zpracování celého notebooku. Ke spuštění je třeba pouze několik základních Python knihoven.
